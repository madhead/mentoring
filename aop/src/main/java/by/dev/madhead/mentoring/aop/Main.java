package by.dev.madhead.mentoring.aop;


import by.dev.madhead.mentoring.aop.config.AOPApplicationConfiguration;
import org.springframework.boot.SpringApplication;

public class Main {
	public static void main(String[] args) {
		SpringApplication.run(AOPApplicationConfiguration.class, args);
	}
}
