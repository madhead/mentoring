package by.dev.madhead.mentoring.aop.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.Map;

public class Exchange {
	private final String currency;
	private final Map<String, String> rates;

	@JsonCreator
	public Exchange(
			@JsonProperty("currency") final String currency,
			@JsonProperty("rates") final Map<String, String> rates
	) {
		this.currency = currency;
		this.rates = rates;
	}

	public String getCurrency() {
		return currency;
	}

	public Map<String, String> getRates() {
		return Collections.unmodifiableMap(rates);
	}
}
