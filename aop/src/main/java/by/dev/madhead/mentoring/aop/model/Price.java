package by.dev.madhead.mentoring.aop.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Price {
	private final String base;
	private final String currency;
	private final String amount;

	@JsonCreator
	public Price(
			@JsonProperty("base") final String base,
			@JsonProperty("currency") final String currency,
			@JsonProperty("amoun") final String amount
	) {
		this.base = base;
		this.currency = currency;
		this.amount = amount;
	}

	public String getBase() {
		return base;
	}

	public String getCurrency() {
		return currency;
	}

	public String getAmount() {
		return amount;
	}
}
