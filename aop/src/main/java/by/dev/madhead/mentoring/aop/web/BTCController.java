package by.dev.madhead.mentoring.aop.web;

import by.dev.madhead.mentoring.aop.model.CoinbaseSpotResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("btc")
public class BTCController {
	private final RestTemplate restTemplate;

	@Autowired
	public BTCController(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@GetMapping
	public String price() {
		return restTemplate
				.getForObject("https://api.coinbase.com/v2/prices/BTC-USD/spot", CoinbaseSpotResponse.class)
				.getData()
				.getAmount();
	}
}
