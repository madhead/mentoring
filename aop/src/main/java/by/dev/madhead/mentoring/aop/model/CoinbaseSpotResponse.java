package by.dev.madhead.mentoring.aop.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CoinbaseSpotResponse {
	private final Price data;

	@JsonCreator
	public CoinbaseSpotResponse(
			@JsonProperty("data") final Price data
	) {
		this.data = data;
	}

	public Price getData() {
		return data;
	}
}
