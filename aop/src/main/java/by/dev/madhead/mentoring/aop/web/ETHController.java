package by.dev.madhead.mentoring.aop.web;

import by.dev.madhead.mentoring.aop.model.CoinbaseSpotResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("eth")
public class ETHController {
	private final RestTemplate restTemplate;

	@Autowired
	public ETHController(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@GetMapping
	public String price() {
		return restTemplate
				.getForObject("https://api.coinbase.com/v2/prices/ETH-USD/spot", CoinbaseSpotResponse.class)
				.getData()
				.getAmount();
	}
}
