package by.dev.madhead.mentoring.aop.web;

import by.dev.madhead.mentoring.aop.model.CoinbaseExchangeResponse;
import by.dev.madhead.mentoring.aop.model.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("exchange")
public class ExchangeController {
	private final RestTemplate restTemplate;

	@Autowired
	public ExchangeController(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@GetMapping("{currency}")
	public Exchange exchange(@PathVariable final String currency) {
		return restTemplate
				.getForObject("https://api.coinbase.com/v2/exchange-rates?currency=" + currency, CoinbaseExchangeResponse.class)
				.getData();
	}
}
