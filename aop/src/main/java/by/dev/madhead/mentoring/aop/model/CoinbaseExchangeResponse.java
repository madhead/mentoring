package by.dev.madhead.mentoring.aop.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CoinbaseExchangeResponse {
	private final Exchange data;

	@JsonCreator
	public CoinbaseExchangeResponse(
			@JsonProperty("data") final Exchange data
	) {
		this.data = data;
	}

	public Exchange getData() {
		return data;
	}
}
