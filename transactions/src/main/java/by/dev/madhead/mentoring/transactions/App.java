package by.dev.madhead.mentoring.transactions;

import java.sql.*;
import java.text.MessageFormat;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class App {
	public static void main(String[] args) throws Exception {
		getConnection()
				.prepareStatement("CREATE TABLE TRANSACTIONS(id INTEGER IDENTITY PRIMARY KEY, value INTEGER NOT NULL)")
				.execute();
		getConnection().prepareStatement("INSERT INTO TRANSACTIONS (id, value) VALUES (1, 1)").execute();
		getConnection().prepareStatement("INSERT INTO TRANSACTIONS (id, value) VALUES (2, 2)").execute();
		getConnection().prepareStatement("INSERT INTO TRANSACTIONS (id, value) VALUES (3, 3)").execute();

		final CountDownLatch done = new CountDownLatch(2);
		final CompletableFuture<Boolean> outOfTurn = new CompletableFuture<>();

		new Thread(() -> {
			try {
				final ResultSet row = getConnection()
						.prepareStatement("SELECT * FROM TRANSACTIONS WHERE id = 2")
						.executeQuery();

				row.next();
				System.out.println(MessageFormat.format(
						"Thread 1 read value: id = {0}, value = {1}",
						row.getInt("id"),
						row.getInt("value")
				));

				outOfTurn.get();

				final PreparedStatement statement = getConnection().prepareStatement("UPDATE TRANSACTIONS SET value " +
						"= 42 WHERE id = ?");

				statement.setInt(1, row.getInt("id"));

				statement.executeUpdate();
			} catch (Exception ignored) {
				ignored.printStackTrace();
			} finally {
				done.countDown();
			}
		}).start();
		new Thread(() -> {
			try {
				getConnection()
						.prepareStatement("UPDATE TRANSACTIONS SET value = value* 2")
						.executeUpdate();
				outOfTurn.complete(true);
			} catch (Exception ignored) {
				ignored.printStackTrace();
			} finally {
				done.countDown();
			}
		}).start();

		done.await();

		try (ResultSet r = getConnection()
				.prepareStatement("SELECT * FROM TRANSACTIONS")
				.executeQuery()) {
			while (r.next()) {
				System.out.println(MessageFormat.format(
						"Value: id = {0}, value = {1}",
						r.getInt("id"),
						r.getInt("value")
				));
			}
		}
	}

	private static Connection getConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:hsqldb:mem:transactions", "sa", "");
	}
}
