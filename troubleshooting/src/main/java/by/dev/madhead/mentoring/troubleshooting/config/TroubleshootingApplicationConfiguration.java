package by.dev.madhead.mentoring.troubleshooting.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableAutoConfiguration
@Import({
		GitRepositorySecretObjectConfiguration.class
})
@ComponentScan(
		basePackages = "by.dev.madhead.mentoring.troubleshooting.web"
)
public class TroubleshootingApplicationConfiguration {
}
