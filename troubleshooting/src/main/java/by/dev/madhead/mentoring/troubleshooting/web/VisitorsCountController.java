package by.dev.madhead.mentoring.troubleshooting.web;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("visitors")
public class VisitorsCountController {
	private Long visitors = 0L;

	@GetMapping
	public Long visitors() {
		visitors = visitors + 1;

		return visitors;
	}

	@DeleteMapping
	public void resetVisitors() {
		visitors = 0L;
	}
}
