package by.dev.madhead.mentoring.troubleshooting.config;

import by.dev.madhead.mentoring.troubleshooting.model.GitRepositorySecretObject;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GitRepositorySecretObjectConfiguration {
	@Bean
	public GitRepositorySecretObject gitRepositorySecretObject() {
		return new GitRepositorySecretObject();
	}
}
