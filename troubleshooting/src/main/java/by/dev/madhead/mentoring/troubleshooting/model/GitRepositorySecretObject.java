package by.dev.madhead.mentoring.troubleshooting.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Value;

public class GitRepositorySecretObject {
	@Value("${TROUBLESHOOTING_SECRET_GIT_REPO_URL:}")
	private String url;

	@Value("${TROUBLESHOOTING_SECRET_GIT_REPO_PUBLIC_KEY:}")
	private String publicKey;

	@Value("${TROUBLESHOOTING_SECRET_GIT_REPO_PRIVATE_KEY:}")
	private String privateKey;

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
				.append("url", url)
				.append("publicKey", publicKey)
				.append("privateKey", privateKey)
				.toString();
	}
}
