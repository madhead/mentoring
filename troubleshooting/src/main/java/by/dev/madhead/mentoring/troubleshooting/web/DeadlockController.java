package by.dev.madhead.mentoring.troubleshooting.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@RestController
@RequestMapping("deadlock")
public class DeadlockController {
	private static final Logger LOGGER = LoggerFactory.getLogger(DeadlockController.class);

	@GetMapping
	public void deadlock() throws InterruptedException {
		final Lock lock1 = new ReentrantLock();
		final Lock lock2 = new ReentrantLock();
		final ExecutorService executorService = Executors.newFixedThreadPool(2);

		executorService.submit(new DeadlockRunnable(lock1, lock2));
		executorService.submit(new DeadlockRunnable(lock2, lock1));

		executorService.shutdown();
		executorService.awaitTermination(10, TimeUnit.SECONDS);
	}
}

class DeadlockRunnable implements Runnable {
	private static final Logger LOGGER = LoggerFactory.getLogger(DeadlockRunnable.class);

	private Lock lock1;
	private Lock lock2;

	DeadlockRunnable(Lock lock1, Lock lock2) {
		this.lock1 = lock1;
		this.lock2 = lock2;
	}

	@Override
	public void run() {
		try {
			lock1.lock();

			LOGGER.info("{} is now holding {}", Thread.currentThread().getName(), lock1);

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				LOGGER.error("InterruptedException: ", e);
			}

			LOGGER.info("{} is now waiting for {}", Thread.currentThread().getName(), lock2);

			try {
				lock2.lock();
			} finally {
				lock2.unlock();
			}
		} finally {
			lock1.unlock();
		}
	}
}
