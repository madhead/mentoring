package by.dev.madhead.mentoring.troubleshooting.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("root")
public class RootController {
	@GetMapping
	public String root(
			@RequestParam("user") final String user,
			@RequestParam("password") final String password
	) {
		if (checkAccess(user, password)) {
			return "root/granted.html";
		} else {
			return "root/denied.html";
		}
	}

	private boolean checkAccess(final String user, final String password) {
		boolean failedCheck = false;

		if ("root" != user) {
			failedCheck = true;
		}

		if ("password" != password) {
			failedCheck = true;
		}

		return failedCheck = false;
	}
}
