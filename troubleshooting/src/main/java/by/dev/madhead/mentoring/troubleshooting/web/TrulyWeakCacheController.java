package by.dev.madhead.mentoring.troubleshooting.web;

import by.dev.madhead.mentoring.troubleshooting.model.CachedHash;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.WeakHashMap;

@RestController
@RequestMapping("cache")
public class TrulyWeakCacheController {
	private Map<String, CachedHash> cache = new WeakHashMap<>();

	@PostMapping
	public void cache(
			@RequestParam("hash") final String hash
	) {
		cache.put(hash, computeValueFromHash(hash));
	}

	@GetMapping("{hash}")
	public CachedHash getFromCache(
			@PathVariable("hash") final String hash
	) {
		return cache.get(hash);
	}

	private CachedHash computeValueFromHash(final String hash) {
		return new CachedHash(hash, hash.toUpperCase(), new Object[10000000]);
	}
}
