package by.dev.madhead.mentoring.troubleshooting.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CachedHash {
	private String hash;
	private String value;

	@JsonIgnore
	private Object[] chunks;

	public CachedHash(String hash, String value, Object[] chunks) {
		this.hash = hash;
		this.value = value;
		this.chunks = chunks;
	}

	public String getHash() {
		return hash;
	}

	public String getValue() {
		return value;
	}

	public Object[] getChunks() {
		return chunks;
	}
}
