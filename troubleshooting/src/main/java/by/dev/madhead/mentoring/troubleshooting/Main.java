package by.dev.madhead.mentoring.troubleshooting;


import by.dev.madhead.mentoring.troubleshooting.config.TroubleshootingApplicationConfiguration;
import org.springframework.boot.SpringApplication;

public class Main {
	public static void main(String[] args) {
		SpringApplication.run(TroubleshootingApplicationConfiguration.class, args);
	}
}
