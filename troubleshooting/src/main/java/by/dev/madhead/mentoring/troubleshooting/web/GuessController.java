package by.dev.madhead.mentoring.troubleshooting.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("guess")
public class GuessController {
	private static final Logger LOGGER = LoggerFactory.getLogger(GuessController.class);

	@Value("${random.int[0,127]}")
	private Integer secret;

	@GetMapping
	public String guess(@RequestParam("guess") final Integer guess) {
		if (guess == secret) {
			return "guess/correct.html";
		} else {
			if (secret < guess) {
				LOGGER.debug("Lower");
			} else {
				LOGGER.debug("Higher");
			}

			return "guess/wrong.html";
		}
	}
}
