A set of playbooks and related stuff to manage VMs for mentees.

# Useful commands

This commands assume that default project and region are saved in `$MAESTRO_CLI_HOME/lib/default.properties`:

    region=EPAM-MSQ3
    project=PERSONAL

## Create an instance

    or2run -i Ubuntu16.04_64-bit -s MINI

## List instances

    or2din

## Tag instance

Tags allow you to distinguish 'mentoring' VMs from others, if you have any.

    or2-set-tag -i ${instanceId} -t mentoring

## Create schedule

Schedules allow you to quotas.

    or2addsch -a start -n mentoring_start_every_morning -c '0 0 7 ? * MON-FRI' -t mentoring
    or2addsch -a stop -n mentoring_stop_every_evening -c '0 0 16 ? * MON-FRI' -t mentoring

## Create Ansible group

    or2ag -a create -g ${groupName}

## List ansible groups

    or2ag

## Include VM in Ansible group

     or2ah -a include -h ${instanceId} -g ${groupName}

## Run playbook

This command assumes your SSH key is added to target VMs, if not, `--user` and `--ask-pass` options are required.

    ./${playbook}.yml -i maestro.sh
